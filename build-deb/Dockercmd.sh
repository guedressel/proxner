#!/bin/bash
set -e

cp -rp /src /build/
cd /build/src

dpkg-buildpackage "$@"

cd /build
rm -rf src
mv * /dist/

exit 0
