#!/usr/bin/env bash

IMG=proxner-dpkg-buildpackage
TAG=latest

if [[ "$(docker images -q ${IMG}:${TAG} 2> /dev/null)" == "" ]]
then
  script_dir=$(dirname "$0")
  echo "Docker image ${IMG}:${TAG} not found" 2>&1
  echo "Build it before like this:" 2>&1
  echo ">  docker build -t ${IMG}:${TAG} ${script_dir}" 2>&1
  exit 1
fi

USER=`id -u`
GROUP=`id -g`

if [[ -t 1 ]]
then
  # running in a (pseudo-)tty
  interact=-it
else
  # no tty around
  interact=-i
fi

exec docker run --rm $interact -u $USER:$GROUP \
  -v $PWD:/src:ro \
  -v $PWD/dist:/dist \
  --tmpfs /build:exec,rw,uid=$USER \
  --tmpfs /tmp \
  ${IMG}:${TAG} \
  /Dockercmd.sh -us -uc -b "$@"

