import logging
from typing import Optional

from proxner import pvesh

logger = logging.getLogger(__name__)


def _alias_hash(alias: dict) -> str:
    return "-".join([alias.get('name', ''), alias.get('cidr', ''), alias.get('comment', '')])


def alias_set(name: str, comment: str, cidr: str):
    result = pvesh.get('cluster/firewall/aliases/')

    existing_alias = False
    for alias in result:
        if alias['name'] == name:
            existing_alias = _alias_hash(alias)
            break

    if not cidr and existing_alias:
        pvesh.delete('cluster/firewall/aliases/%s' % name)
        return

    if not existing_alias:
        pvesh.create('cluster/firewall/aliases', [('name', name), ('comment', comment), ('cidr', cidr)])
    elif existing_alias != _alias_hash({'name': name, 'comment': comment, 'cidr': cidr}):
        pvesh.set('cluster/firewall/aliases/%s' % name, [('comment', comment), ('cidr', cidr)])


def _ipset_entry_hash(rule: dict) -> str:
    return "-".join([
        rule.get('cidr'), rule.get('comment'), "%s" % rule.get('nomatch', False)
    ])


def ipset_set(name: str, comment: str, data: [dict]) -> Optional[list]:
    result = pvesh.get('cluster/firewall/ipset/%s' % name)

    if not data:
        if isinstance(result, list):
            pvesh.delete('cluster/firewall/ipset/%s' % name)
        return

    entries_control_list = dict()
    if not isinstance(result, list):
        pvesh.create('cluster/firewall/ipset', [('name', name), ('comment', comment)])
    else:
        for item in result:
            entries_control_list[item.get('cidr')] = _ipset_entry_hash(item)

    for item in data:
        cidr = item.get('cidr', None)
        if not cidr:
            continue
        set_data = []
        if 'comment' in item:
            set_data.append(('comment', item.get('comment')))
        if 'nomatch' in item:
            set_data.append(('nomatch', 1 if item.get('nomatch') else 0))

        if cidr not in entries_control_list.keys():
            set_data.append(('cidr', cidr))
            pvesh.create('cluster/firewall/ipset/%s' % name, set_data)
        elif entries_control_list[cidr] != _ipset_entry_hash(item):
            pvesh.set('cluster/firewall/ipset/%s/%s' % (name, cidr), set_data)

    # remove obsolete entries
    current_cidrs = [i.get('cidr') for i in data]
    for cidr in entries_control_list.keys():
        if cidr not in current_cidrs:
            pvesh.delete('cluster/firewall/ipset/%s/%s' % (name, cidr))


def rule_dict(pos: int = None, type: str = '', action: str = '', comment: str = '', source: str = '', dest: str = '', macro: str = '', proto: str = '', sport: int = None, dport: int = None, enable: bool = True) -> dict:
    return {
        'pos': pos if isinstance(pos, int) else '',
        'type': type,
        'action': action,
        'comment': comment,
        'source': source,
        'dest': dest,
        'macro': macro,
        'proto': proto,
        'sport': sport if isinstance(sport, int) else '',
        'dport': dport if isinstance(dport, int) else '',
        'enable': 1 if enable else 0
    }


def _rule_hash(rule: dict) -> str:
    return "-".join([
        rule.get('type'), rule.get('action'), rule.get('source', ''), rule.get('dest', ''),
        "%s" % rule.get('sport', ''), "%s" % rule.get('dport', ''), rule.get('proto', ''), rule.get('macro', ''),
        "%s" % rule.get('enable', '')
    ])


def group_set(name: str, comment: str, data: [dict]):
    result = pvesh.get('cluster/firewall/groups/%s' % name)

    if not data:
        if result:
            pvesh.delete('cluster/firewall/groups/%s' % name)
        return

    rule_control_list = dict()
    if not isinstance(result, list):
        pvesh.create('cluster/firewall/groups', [('group', name), ('comment', comment)])
    else:
        for item in result:
            rule_control_list[item.get('pos')] = _rule_hash(item)

    for item in data:
        pos = item.get('pos')
        if rule_control_list.get(pos, None) == _rule_hash(item):
            # rule already exists.
            continue

        # create or update rule
        rule_data = list()
        for k, v in item.items():
            if k != 'pos':
                rule_data.append((k, v))
        if pos in rule_control_list.keys():
            pvesh.set('cluster/firewall/groups/%s/%s' % (name, pos), rule_data)
        else:
            rule_data.append(('pos', pos))
            pvesh.create('cluster/firewall/groups/%s' % name, rule_data)

    # remove obsolete rules
    current_positions = [i.get('pos') for i in data]
    for pos in rule_control_list.keys():
        if pos not in current_positions:
            pvesh.delete('cluster/firewall/groups/%s/%s' % (name, pos))


def rules_maintain(rules: [dict]):
    result = pvesh.get('cluster/firewall/rules')

    # FIXME: Seems there is a bug in maintaining the right rule positions. Not yet investigated...

    if not rules:
        return

    rule_control_list = dict()
    if isinstance(result, list):
        for item in result:
            rule_control_list[item.get('pos')] = _rule_hash(item)

    for rule in rules:
        pos = rule.get('pos')
        rule_hash = _rule_hash(rule)
        if rule_control_list.get(pos, None) == rule_hash:
            # rule already exists at correct position.
            continue

        rule_current_pos = None
        for i, h in rule_control_list.items():
            if h == rule_hash:
                # found rule at wrong position
                rule_current_pos = i
                continue
        # if rule_current_pos:  # FIXME: move rule doesn't work this way (API error). Also a racecondition would have overwritten rule 1 before it would have been moved.
        #     # move rule:
        #     pvesh.set('cluster/firewall/rules/%s' % rule_current_pos, [('pos', pos)])
        #     continue

        # create or update rule
        rule_data = list()
        for k, v in rule.items():
            if k != 'pos':
                rule_data.append((k, v))
        if pos in rule_control_list.keys():
            pvesh.set('cluster/firewall/rules/%s' % pos, rule_data)
        else:
            rule_data.append(('pos', pos))
            pvesh.create('cluster/firewall/rules', rule_data)


