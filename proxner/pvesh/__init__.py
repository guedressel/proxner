import json
import logging
import re
import shutil
import subprocess

from proxner.pvesh import firewall

logger = logging.getLogger(__name__)

pvesh_available = shutil.which('pvesh')

if not pvesh_available:
    logger.warning("Command pvesh not found")

    def _exec_pvesh(cmd: str, path: str, data: [(str, str)] = None) -> (any, str):
        logger.warning("pvesh command not available")
        return None, "pvesh command not available"

else:

    def _exec_pvesh(cmd: str, path: str, data: [(str, str)] = None) -> (any, str):
        try:
            params = []
            if data:
                for k, v in data:
                    if k is None or v is None:
                        continue
                    params.append("--%s" % k)
                    params.append("%s" % v)
            params.append('--output-format')
            params.append('json')
            command = ['pvesh', cmd, path] + params
            logger.debug("execute %s" % " ".join(command))
            output = subprocess.check_output(command, stderr=subprocess.PIPE)
            if output:
                return json.loads(output.decode("utf-8")), ""
            else:
                return "", ""
        except subprocess.CalledProcessError as e:
            # logger.error(e.stderr)
            return e.stdout.decode('utf-8'), e.stderr.decode('utf-8')


def command_available() -> bool:
    return pvesh_available


def get(path):
    out, err = _exec_pvesh('get', path)
    if err:
        if re.match(r'^no such .+ \'.+\'\n$', err):
            # Not found
            return None
        else:
            logger.debug("pvesh command stderr: %s" % err)
            return False
    else:
        return out


def ls(path):
    out, err = _exec_pvesh('ls', path)
    if err:
        logger.debug("pvesh command stderr: %s" % err)
        return False
    else:
        return out


def create(path, data: [(str, str)]):
    out, err = _exec_pvesh('create', path, data)
    if err:
        logger.debug("pvesh command stderr: %s" % err)
        return False
    else:
        return out


def set(path, data: [(str, str)]):
    out, err = _exec_pvesh('set', path, data)
    if err:
        logger.debug("pvesh command stderr: %s" % err)
        return False
    else:
        return out


def delete(path):
    out, err = _exec_pvesh('delete', path)
    if err:
        logger.debug("pvesh command stderr: %s" % err)
        return False
    else:
        return out

