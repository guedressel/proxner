import logging
import os
import re
from typing import Optional

from proxner import config

logger = logging.getLogger(__name__)


def truncate_vowel(string):
    return re.sub("[AaEeIiOoUu]+", "", string)


def bridged_iface_name(bridge_name: str, remote_node_name: str) -> Optional[str]:
    name = "%s-%s" % (bridge_name, remote_node_name)
    if len(name) > 13:
        name = "%s-%s" % (bridge_name, truncate_vowel(remote_node_name))
    if len(name) > 13:
        logger.error("Interface name is too long: %s" % name)
        return None
    return name


def match_bridged_iface_name(bridge_name: str, iface_name: str) -> bool:
    return re.fullmatch(r"^%s-.+$" % re.escape(bridge_name), iface_name) is not None


def tun_iface_names(bridge_name: str, local_node: str, remote_nodes: [str]) -> {str: str}:
    result = {}
    for remote_node in remote_nodes:
        if remote_node == local_node:
            continue
        iface = bridged_iface_name(bridge_name, remote_node)
        if not iface:
            return None
        if iface in result.items():
            logger.error("Duplicate interface name: %s" % iface)
            return None
        result[remote_node] = iface
    return result


def vnet_tun_names(vnet_name: str, local_node: str) -> {str: str}:
    return tun_iface_names(vnet_name, local_node, vnet_tunnel_remote_nodes(vnet_name))


def vnet_tunnel_remote_nodes(vnet_name: str) -> {str: str}:
    excludes = config.vnet_excluded_nodes(vnet_name)
    return [node for node in config.node_names() if node not in excludes]


def pve_tun_names(local_node: str) -> {str: str}:
    return tun_iface_names(config.pve_cluster_net_name(), local_node, config.pve_node_names())


def bridged_interfaces(bridge_name: str) -> [str]:
    interfaces = os.listdir('/sys/class/net/')

    for bridged_iface in [iface for iface in interfaces if os.path.exists("/sys/class/net/%s/master" % iface)]:
        iface_bridge_name = os.path.basename(os.readlink("/sys/class/net/%s/master" % bridged_iface))
        if iface_bridge_name == bridge_name:
            yield bridged_iface


def network_geneve_vni(net_name: str) -> int:
    vni = 0
    for c in net_name.upper():
        vni += ord(c) - 47  # 0 is 48; by reducing ord by 47 the vni values don't get too high.
    # TODO: check for vni > 0 and vni < max vni (whatever that number is)
    return vni


def tun_up_commands(bridge_name, remote_node_name):
    iface = bridged_iface_name(bridge_name, remote_node_name)
    if not iface:
        return None
    vni = network_geneve_vni(bridge_name)
    if not vni:
        return None

    ip = config.node_ip4(remote_node_name)
    iproute2_link_add_cmd = ["ip", "link", "add", "dev", iface, "type", "geneve", "remote", ip, "vni",
                             "%d" % vni]
    iproute2_addrgenmode_cmd = ["ip", "link", "set", "dev", iface, "addrgenmode", "none"]
    iproute2_link_up_cmd = ["ip", "link", "set", "dev", iface, "up"]

    return [
        iproute2_link_add_cmd,
        iproute2_addrgenmode_cmd,
        iproute2_link_up_cmd
    ]


def iface_del_commands(iface_name):
    return [["ip", "link", "del", iface_name]]
