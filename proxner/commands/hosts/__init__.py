import argparse
import logging

logger = logging.getLogger(__name__)


def init_argument_parser(parser: argparse.ArgumentParser) -> None:
    parser.description = 'Generate hosts file'


def run(args: argparse.Namespace) -> int:
    logger.debug("Command: Generate hosts file")
    from proxner.commands.hosts.command import generate_hosts
    return generate_hosts(args)

