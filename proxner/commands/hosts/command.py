import argparse
import logging

from proxner import template, config

logger = logging.getLogger(__name__)


def generate_hosts(args: argparse.Namespace) -> int:

    if not args.node:
        logger.error("No node name provided.")
        return 1

    local_node = args.node
    data = dict()

    data['pub_hosts'] = list()
    for node in config.node_names():
        node_fqdn = config.node_fqdn(node)
        if not node_fqdn:
            continue
        node_ip6 = config.node_ip6(node)
        node_ip4 = config.node_ip4(node)
        data['pub_hosts'].append((node_ip6, [node_fqdn]))
        data['pub_hosts'].append((node_ip4, [node_fqdn]))

    if local_node in config.pve_node_names():
        data['pve_hosts'] = list()
        for node in config.pve_node_names():
            pve_ip4 = config.pve_node_address_ip4(node)
            node_hostname = config.node_host(node)
            pve_fqdn = config.node_cluster_fqdn(node)
            names = [pve_fqdn, node_hostname]
            data['pve_hosts'].append((pve_ip4, names))

    print(template.render('hosts.jinja', data))
    return 0

