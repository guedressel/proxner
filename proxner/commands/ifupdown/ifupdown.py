import logging
import os

from debinterface import Interfaces
from netaddr import IPNetwork, IPAddress

import proxner.config as config

logger = logging.getLogger(__name__)


def _set_adapter(interfaces: Interfaces, iface_name: str, family: str, network: IPNetwork, address: IPAddress,
                 set_bridge_opts=True) -> bool:

    family_readable = 'IPv6' if family == 'inet6' else 'IPv4'
    adapter = None
    for i, a in enumerate(interfaces.adapters):
        if a.get_attr('name') == iface_name and a.get_attr('addrFam') == family:
            adapter = a
            break

    if not adapter:
        logger.debug("Creating new adapter %s (%s)" % (iface_name, family_readable))
        adapter = interfaces.addAdapter({
            'name': iface_name,
            'addrFam': family,
            'auto': True,
            'source': 'manual'
        })

    orig_adapter_state = str(adapter.export())
    adapter.setUnknown('proxner', 'true')

    if set_bridge_opts:
        adapter.setBropts({
            'ports': 'none',
            'stp': 'on',
            'hello': '1',
            'fd': '2'
        })
        mtu = '1472'  # vnet tunnels require lower MTU because of encapsulated network package headers
        post_up = []
        if iface_name == 'pve':
            post_up.append('/usr/sbin/proxner vnet pve-up')
            adapter.setPreDown(['/usr/sbin/proxner vnet pve-down'])
        else:
            # vent tunnels which probably span over hetzner vhosts (gw-nodes) need a even lower MTU
            # to perform on multi tunneled connections
            mtu = '1448'
            post_up.append('/usr/sbin/proxner vnet vnet-up %s' % iface_name)
            adapter.setPreDown(['/usr/sbin/proxner vnet vnet-down %s' % iface_name])

        # setting MTU via post_up hook - following bug report seems to describe issues with attribute 'mtu' in
        # interfaces config:
        # https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1399064
        post_up.append('/sbin/ip link set dev %s mtu %s' % (iface_name, mtu))
        adapter.setPostUp(post_up)
    else:
        adapter.setBropts({})
        adapter.setPostUp([])
        adapter.setPreDown([])

    if address:
        adapter.setAddressSource('static')
        adapter.setAddress(str(address))
        adapter.setNetmask(str(network.netmask))

    new_adapter_state = str(adapter.export())

    if new_adapter_state != orig_adapter_state:
        logger.debug("Network interface %s (%s) modified" % (iface_name, family_readable))
        adapter.setUnknown('# managed', 'by proxner')
        if os.path.exists("/sys/class/net/%s" % iface_name):
            logger.info("Shutting down modified network interface %s" % iface_name)
            interfaces.downAdapter(iface_name)
        return True
    else:
        logger.debug("Network interface %s (%s) up-to-date" % (iface_name, family_readable))
        return False


def run_ifupdown(local_node: str) -> int:

    interfaces = Interfaces()  # type: Interfaces
    vnet_names = []
    for vnet_name in config.vnet_names():
        if local_node not in config.vnet_excluded_nodes(vnet_name):
            vnet_names.append(vnet_name)

    dirty = False

    is_pve_node = False
    if local_node in config.pve_node_names():
        is_pve_node = True
        net4 = config.pve_cluster_net_addr()
        addr4 = config.pve_node_address_ip4(local_node)
        if _set_adapter(interfaces, 'pve', 'inet', net4, addr4, True):
            dirty = True
    elif local_node not in config.gw_node_names():
        logging.error("Invalid node name %s" % local_node)
        return 1

    # detect obsolete vnet interfaces
    for i, a in enumerate(interfaces.adapters):
        if 'proxner' not in a.attributes.get('unknown', []):
            # skip adapters not created by proxner
            continue
        iface = a.get_attr('name')
        if is_pve_node and iface.startswith('pve'):
            # don't touch pve cluster interfaces if run on a pve node
            continue

        family = 6 if a.get_attr('addrFam') == 'inet6' else 4

        delete = False
        if iface not in vnet_names:
            delete = True
        elif family == 6 and not config.vnet_ip6_network(iface):
            delete = True
        elif family == 4 and not config.vnet_ip4_network(iface):
            delete = True

        if delete:
            logger.debug("Removing network interface %s [%i, IP%i] from /etc/network/interfaces" % (iface, i, family))
            interfaces.removeAdapter(i)
            if os.path.exists("/sys/class/net/%s" % iface):
                logger.info("Shutting down removed network interface %s" % iface)
                interfaces.downAdapter(iface)
            dirty = True

    for vnet_name in vnet_names:
        net6 = config.vnet_ip6_network(vnet_name)
        bridge_opts_on_inet6 = bool(net6)
        if net6:
            addr6 = config.vnet_node_address_ip6(vnet_name, local_node)
            if _set_adapter(interfaces, vnet_name, 'inet6', net6, addr6, bridge_opts_on_inet6):
                dirty = True

        net4 = config.vnet_ip4_network(vnet_name)
        if net4:
            addr4 = config.vnet_node_address_ip4(vnet_name, local_node)
            if _set_adapter(interfaces, vnet_name, 'inet', net4, addr4, not bridge_opts_on_inet6):
                dirty = True

    if dirty:
        logger.info("/etc/network/interfaces updated")
        interfaces.writeInterfaces()
    else:
        logger.info("/etc/network/interfaces up-to-date")

    ifaces = [] + vnet_names
    if is_pve_node:
        ifaces += ['pve']
    for iface in ifaces:
        if not os.path.exists("/sys/class/net/%s" % iface):
            logger.info("Bringing up network interface %s" % iface)
            interfaces.upAdapter(iface)

    return 0
