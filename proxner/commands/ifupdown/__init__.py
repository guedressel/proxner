import argparse
import logging

logger = logging.getLogger(__name__)


def init_argument_parser(parser: argparse.ArgumentParser) -> None:
    parser.description = 'Generate ifupdown configuration'


def run(args: argparse.Namespace) -> int:
    from proxner.commands.ifupdown.ifupdown import run_ifupdown
    logger.debug("Command: generate ifupdown configuration")
    try:
        return run_ifupdown(args.node)
    except Exception as e:
        logger.error(str(e))
        return 1
