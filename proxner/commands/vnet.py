import argparse
import os.path
import subprocess
import logging
from proxner import config, network

logger = logging.getLogger(__name__)


def _exec_cmds(commands: [], dry_run: bool=False) -> bool:
    if not commands:
        return False
    if dry_run:
        for command in commands:
            logger.debug("[DRY RUN] " + " ".join(command))
    else:
        try:
            for command in commands:
                subprocess.check_output(command)
        except subprocess.CalledProcessError as e:
            logger.fatal(e.output)
            return False
    return True


def _bridge_tunnels_down(bridge_name: str, dry_run: bool=False) -> bool:
    logger.info("Bringing down interfaces of bridge %s" % bridge_name)

    for iface in network.bridged_interfaces(bridge_name):
        logger.info("Deleting bridged interface %s" % iface)
        commands = network.iface_del_commands(iface)
        if not _exec_cmds(commands, dry_run):
            return False

    interfaces = os.listdir('/sys/class/net/')
    for iface in [iface for iface in interfaces if network.match_bridged_iface_name(bridge_name, iface)]:
        logger.info("Deleting interface %s (non bridge, but matching bridge's name pattern)" % iface)
        commands = network.iface_del_commands(iface)
        if not _exec_cmds(commands, dry_run):
            return False

    return True


def _bridge_tunnels_up(bridge_name: str, local_node: str, remote_nodes: [str], dry_run: bool=False) -> bool:
    logger.info("Bringing up tunnel interfaces for bridge %s" % bridge_name)
    tun_names_ = network.tun_iface_names(bridge_name, local_node, remote_nodes)
    if not tun_names_:
        logger.info("No configuration of tunnel interfaces for bridge %s found" % bridge_name)
        return True

    if not os.path.exists("/sys/class/net/%s" % bridge_name):
        logger.error("Bridge interface %s does not exist" % bridge_name)
        return _bridge_tunnels_down(bridge_name, dry_run)  # do cleanup.
    elif not os.path.exists("/sys/class/net/%s/bridge" % bridge_name):
        logger.fatal("Interface %s is not a bridge" % bridge_name)
        _bridge_tunnels_down(bridge_name, dry_run)  # do cleanup.
        return False

    existing_tun_interfaces = network.bridged_interfaces(bridge_name)
    for obsolete_iface in [iface for iface in existing_tun_interfaces if iface not in tun_names_.values()]:
        logger.info("Delete obsolete tunnel %s" % obsolete_iface)
        del_cmd = network.iface_del_commands(obsolete_iface)
        if not _exec_cmds(del_cmd, dry_run):
            return False

    for node_name, iface in tun_names_.items():
        if os.path.exists("/sys/class/net/%s" % iface):
            logger.debug("Tunnel interface %s already exists" % iface)
        else:
            logger.info("Creating tunnel interface %s" % iface)
            commands = network.tun_up_commands(bridge_name, node_name)
            if not _exec_cmds(commands, dry_run):
                return False

    existing_tun_interfaces = network.bridged_interfaces(bridge_name)
    for iface in tun_names_.values():
        if iface in existing_tun_interfaces:
            logger.debug("Tunnel interface %s already bridged by %s" % (iface, bridge_name))
            continue
        logger.info("Adding tunnel interface %s to bridge %s" % (iface, bridge_name))
        bridge_addif_command = ['ip', 'link', 'set', 'dev', iface, 'master', bridge_name]
        if not _exec_cmds([bridge_addif_command], dry_run):
            return False

    return True


def vnet_up(args: argparse.Namespace) -> int:
    if args.vnet:
        networks = [args.vnet]
    else:
        networks = config.vnet_names()

    local_node = args.node

    for vnet_name in networks:
        if not config.vnet_defined(vnet_name):
            logger.fatal("vnet %s not configured" % vnet_name)
            return 1

    for vnet_name in networks:
        logger.info("Bringing up vnet %s tunnels" % vnet_name)
        remote_nodes = network.vnet_tunnel_remote_nodes(vnet_name)
        if not _bridge_tunnels_up(vnet_name, local_node, remote_nodes, args.dry_run):
            return 1

    return 0


def vnet_down(args: argparse.Namespace) -> int:
    if args.vnet:
        networks = [args.vnet]
    else:
        networks = config.vnet_names()
    for vnet_name in networks:
        if not config.vnet_defined(vnet_name):
            logger.fatal("vnet %s not configured" % vnet_name)
            return 1

    for vnet_name in networks:
        logger.info("Bringing down vnet %s tunnels" % vnet_name)
        if not _bridge_tunnels_down(vnet_name, args.dry_run):
            return 1

    return 0


def pve_up(args):
    net_name = config.pve_cluster_net_name()
    local_node = args.node
    logger.info("Bringing up pve tunnels")
    return 0 if _bridge_tunnels_up(net_name, local_node, config.pve_node_names(), args.dry_run) else 1


def pve_down(args: argparse.Namespace) -> int:
    net_name = config.pve_cluster_net_name()
    logger.info("Bringing down pve tunnels")
    return 0 if _bridge_tunnels_down(net_name, args.dry_run) else 1


def init_argument_parser(parser):
    # type (argparse.ArgumentParser) -> None
    parser.description = 'Control vnet geneve tunnels'

    action_parsers = parser.add_subparsers(dest='action',
                                           help='Action to perform for vnet interface.')
    action_parsers.required = True

    up_parser = action_parsers.add_parser('vnet-up', help='Bring up tunnel interfaces of vnet')
    up_parser.add_argument('vnet', metavar='VNET', type=str, nargs='?',
                           help='network name (default: all networks)')
    up_parser.add_argument('--dry-run', action="store_true", dest='dry_run',
                           help="Don't actually create network interfaces")

    down_parser = action_parsers.add_parser('vnet-down', help='Tear down tunnel interfaces of vnet')
    down_parser.add_argument('vnet', metavar='VNET', type=str, nargs='?',
                             help='network name (default: all networks)')
    down_parser.add_argument('--dry-run', action="store_true", dest='dry_run',
                             help="Don't actually delete network interfaces")

    pve_up_parser = action_parsers.add_parser('pve-up', help='Bring up tunnel interfaces of pve cluster net')
    pve_up_parser.add_argument('--dry-run', action="store_true", dest='dry_run',
                               help="Don't actually create network interfaces")

    pve_down_parser = action_parsers.add_parser('pve-down', help='Tear down tunnel interfaces of pve cluster net')
    pve_down_parser.add_argument('--dry-run', action="store_true", dest='dry_run',
                                 help="Don't actually delete network interfaces")


def run(args: argparse.Namespace) -> int:
    vnet = args.vnet if hasattr(args, 'vnet') and args.vnet else '*'
    logger.info("[%s] vnet %s %s" % (args.node, "%s-*" % vnet, args.action))

    if args.action == 'vnet-up':
        return vnet_up(args)
    elif args.action == 'vnet-down':
        return vnet_down(args)
    elif args.action == 'pve-up':
        return pve_up(args)
    elif args.action == 'pve-down':
        return pve_down(args)
    else:
        logger.fatal("Invalid action")
        return 1
