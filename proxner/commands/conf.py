
import logging
import proxner.config as config
import argparse

logger = logging.getLogger(__name__)


def init_argument_parser(parser: argparse.ArgumentParser) -> None:
    parser.description = 'Inspect configuration'
    action_parsers = parser.add_subparsers(dest='action', help='Action')
    list_action = action_parsers.add_parser('list')
    list_action.add_argument('list_type', metavar='type', choices=['node', 'vnet'])
    pass


def run(args: argparse.Namespace) -> int:
    logger.debug("Command: inspect configuration")
    if args.action == 'list':
        if args.list_type == 'node':
            for node_name in config.node_names():
                print(node_name)
            return 0
        elif args.list_type == 'vnet':
            for vnet_name in config.vnet_names():
                print(vnet_name)
            return 0
    else:
        logger.fatal('Invalid action: %s' % args.action)
        return 1


