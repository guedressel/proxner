
import logging
import proxner.config as config
import proxner.template as template
from collections import OrderedDict

logger = logging.getLogger(__name__)


def ipsec_conf(node_name):
    # type (str) -> str
    from_node = node_name
    from_ip4 = config.node_ip4(from_node)
    if not from_ip4:
        logger.fatal("Invalid configuration: Node %s has empty pubIP4" % from_node)
        return False
    from_ip6 = config.node_ip6(from_node)
    if not from_ip6:
        logger.fatal("Invalid configuration: Node %s has empty pubIP6" % from_node)
        return False

    connections = list()
    nodes = config.node_names()
    for to_node in nodes:
        if from_node == to_node:
            continue
        to_ip4 = config.node_ip4(to_node)
        if not to_ip4:
            logger.fatal("Invalid configuration: Node %s has empty pubIP4" % from_node)
            return False
        conn_data = OrderedDict()
        conn_data['left'] = from_ip4
        conn_data['leftid'] = "@%s" % from_node
        conn_data['right'] = to_ip4
        conn_data['rightid'] = "@%s" % to_node
        connections.append({
            'name': "%s-%s-ip4" % (from_node, to_node),
            'data': sorted(conn_data.items(), key=lambda t: t[0])
        })
        to_ip6 = config.node_ip6(to_node)
        if not to_ip6:
            logger.fatal("Invalid configuration: Node %s has empty pubIP6" % from_node)
            return False
        conn_data = OrderedDict()
        conn_data['left'] = from_ip6
        conn_data['leftid'] = "@%s" % from_node
        conn_data['right'] = to_ip6
        conn_data['rightid'] = "@%s" % to_node
        connections.append({
            'name': "%s-%s-ip6" % (from_node, to_node),
            'data': sorted(conn_data.items(), key=lambda t: t[0])
        })

    if not connections:
        logger.info("No ipsec connections")

    default_connection_data = config.ipsec_conn_directives()
    default_data = sorted(default_connection_data.items(), key=lambda t: t[0])

    return template.render('ipsec.conf.jinja', {'defaults': default_data, 'connections': connections})


def ipsec_secrets(node_name):
    # type (str) -> str
    psk_secrets = []

    from_node = node_name
    from_node_psk_half = config.node_ipsec_half_secret(from_node)
    from_node_index = config.node_index(from_node)
    if not from_node_psk_half:
        logger.fatal("No IPsec pre-shared-key part configured for node %s" % from_node)
        return False
    nodes = config.node_names()
    for to_node in nodes:
        if from_node == to_node:
            continue
        to_node_psk_half = config.node_ipsec_half_secret(to_node)
        if not to_node_psk_half:
            logger.fatal("No IPsec pre-shared-key part configured for node %s" % to_node)
            return False
        to_node_index = config.node_index(to_node)
        if from_node_index < to_node_index:
            psk = "%s%s" % (from_node_psk_half, to_node_psk_half)
        else:
            psk = "%s%s" % (to_node_psk_half, from_node_psk_half)
        psk_secrets.append({
            'id': "@%s" % to_node,
            'psk': psk,
        })

    return template.render('ipsec.secrets.jinja', {'psk_secrets': psk_secrets})


def init_argument_parser(parser):
    # type (argparse.ArgumentParser) -> None
    parser.add_argument('type', choices=('secrets', 'conf'),
                        help='Type of config file')


def run(args):
    # type (argparse.Namespace) -> int
    if args.type == 'conf':
        logger.debug("Command: generate ipsec.conf")
        c = ipsec_conf(args.node)
        if c:
            print(c)
            return 0
        else:
            return 1

    elif args.type == 'secrets':
        logger.debug("Command: generate ipsec.secrets")
        s = ipsec_secrets(args.node)
        if s:
            print(s)
            return 0
        else:
            return 1

