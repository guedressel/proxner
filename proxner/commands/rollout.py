import contextlib
import io
import logging
from os import path
import subprocess
from enum import Enum
import argparse

from proxner.commands import ipsec, ifupdown, hosts

logger = logging.getLogger(__name__)


class WriteResult(Enum):
    CREATED = 1
    CHANGED = 2
    UNCHANGED = 3


def restart_systemd_service(service_name):
    try:
        subprocess.check_output(['systemctl', 'restart', service_name])
    except subprocess.CalledProcessError as e:
        logger.fatal(e.output)
        return 1


def write_file(file_path: str, content: str) -> WriteResult:
    if path.exists(file_path):
        current_content = open(file_path, 'r').read()
        if content == current_content:
            logger.info("%s up to date." % file_path)
            return WriteResult.UNCHANGED
        else:
            open(file_path, 'w').write(content)
            logger.info("%s updated." % file_path)
            return WriteResult.CHANGED
    else:
        open(file_path, 'w').write(content)
        logger.info("%s created." % file_path)
        return WriteResult.CREATED


def init_argument_parser(parser: argparse.ArgumentParser) -> None:
    parser.description = 'Inspect configuration'
    action_parsers = parser.add_subparsers(dest='action', help='Action')
    action_parsers.required = True
    configs_action = action_parsers.add_parser('configs')
    pass


def run(args: argparse.Namespace) -> int:
    logger.debug("Command: rollout")
    if args.action == 'configs':
        stdout = io.StringIO()
        with contextlib.redirect_stdout(stdout):
            if ipsec.run(argparse.Namespace(type='conf', node=args.node)) != 0:
                stdout.close()
                logger.error('Failed to generate ipsec.conf')
                return 1

        ipsec_conf = stdout.getvalue()
        stdout.close()

        stdout = io.StringIO()
        with contextlib.redirect_stdout(stdout):
            if ipsec.run(argparse.Namespace(type='secrets', node=args.node)) != 0:
                stdout.close()
                logger.error('Failed to generate ipsec.secrets')
                return 1
        ipsec_secrets = stdout.getvalue()
        stdout.close()

        stdout = io.StringIO()
        with contextlib.redirect_stdout(stdout):
            if hosts.run(argparse.Namespace(node=args.node)) != 0:
                stdout.close()
                logger.error('Failed to generate hosts file')
                return 1
        hosts_file = stdout.getvalue()
        stdout.close()

        write_file('/etc/hosts', hosts_file)

        restart_ipsec = False
        if write_file('/etc/ipsec.conf', ipsec_conf) is not WriteResult.UNCHANGED:
            restart_ipsec = True
        if write_file('/etc/ipsec.secrets', ipsec_secrets) is not WriteResult.UNCHANGED:
            restart_ipsec = True
        if restart_ipsec:
            logger.info('IPsec config changed: Restarting strongswan')
            restart_systemd_service('strongswan')

        if ifupdown.run(argparse.Namespace(node=args.node)) != 0:
            logger.error('Failed to generate /etc/network/interfaces')
            return 1

        return 0
    else:
        logger.fatal('Invalid action: %s' % args.action)
        return 1
