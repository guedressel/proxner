import argparse
import logging

logger = logging.getLogger(__name__)


def init_argument_parser(parser: argparse.ArgumentParser) -> None:
    parser.description = 'Proxmox API'
    parser.add_argument('action', choices=('debug', 'firewall'),
                        help='API action')


def run(args: argparse.Namespace) -> int:
    from proxner import pvesh
    if not pvesh.command_available():
        logger.error("Command pvesh not found. Is Proxmox VE installed?")
        return 1

    if args.action == 'debug':
        logger.debug("Proxmox API: debug")
        from proxner.commands.pve.pve import run_debug
        return run_debug()
    elif args.action == 'firewall':
        logger.debug("Proxmox API: firewall")
        from proxner.commands.pve.pve import run_firewall
        return run_firewall()
    else:
        logger.fatal("Invalid action %s" % args.action)
        return 1


