import logging

from proxner import config, pvesh

logger = logging.getLogger(__name__)


def run_debug():
    # cluster_net = config.pve_cluster_net_addr()
    # print("ipset cluster_net")
    # pvesh.firewall.ipset(cluster_net)
    return 0


def run_firewall():
    # IP Set 'pve-nodes' may include all pve-node public addresses
    ipset_pve_nodes = list()
    for node_name in config.pve_node_names():
        host = config.node_host(node_name)
        if not host:
            logger.debug("Invalid node definition for %s. Skipping node" % node_name)
            continue
        ip6 = config.node_ip6(node_name)
        if ip6:
            ipset_pve_nodes.append({'cidr': "%s" % ip6, 'comment': "%s IPv6" % host, 'nomatch': False})
        ip4 = config.node_ip4(node_name)
        if ip4:
            ipset_pve_nodes.append({'cidr': "%s" % ip4, 'comment': "%s IPv4" % host, 'nomatch': False})
    ipset_pve_nodes = sorted(ipset_pve_nodes, key=lambda i: i['cidr'])
    pvesh.firewall.ipset_set('pve-nodes', '#proxner - auto generated', ipset_pve_nodes)

    # IP Set 'gw-nodes' may include all gw-node public addresses
    ipset_gw_nodes = list()
    for node_name in config.gw_node_names():
        host = config.node_host(node_name)
        if not host:
            logger.debug("Invalid node definition for %s. Skipping node" % node_name)
            continue
        ip6 = config.node_ip6(node_name)
        if ip6:
            ipset_gw_nodes.append({'cidr': "%s" % ip6, 'comment': "%s IPv6" % host, 'nomatch': False})
        ip4 = config.node_ip4(node_name)
        if ip4:
            ipset_gw_nodes.append({'cidr': "%s" % ip4, 'comment': "%s IPv4" % host, 'nomatch': False})
    ipset_gw_nodes = sorted(ipset_gw_nodes, key=lambda i: i['cidr'])
    pvesh.firewall.ipset_set('gw-nodes', '#proxner - auto generated', ipset_gw_nodes)

    # PVE-Magic IP Set 'management' may include all pve-management-networks CIDRs to allow access to PVE WebUI
    ipset_management = list()
    for comment, net in config.pve_management_net():
        if net.first == net.last:
            # Host address in management network definitions (IP4 /32 or IP6 /128)
            net = net.network
        ipset_management.append({'cidr': "%s" % net, 'comment': comment, 'nomatch': False})
    pvesh.firewall.ipset_set('management', '#proxner - auto generated', ipset_management)

    # Alias 'pve-cluster' may describe cluster network
    # see https://pve.proxmox.com/wiki/Firewall#pve_firewall_ip_aliases
    pvesh.firewall.alias_set('local_network', '#proxner - auto generated', "%s" % config.pve_cluster_net_addr())

    # Security Group (rule set) 'pve-cluster' may allow ipsec geneve tunnel traffic between pve-nodes
    rule0 = pvesh.firewall.rule_dict(pos=0, type='in', action='ACCEPT', comment='IPsec traffic',
                                     source='+pve-nodes', dest='+pve-nodes', macro='IPsec')
    rule1 = pvesh.firewall.rule_dict(pos=1, type='in', action='ACCEPT', comment='Geneve tunnel traffic',
                                     source='+pve-nodes', dest='+pve-nodes', proto='udp', dport=6081)
    rule2 = pvesh.firewall.rule_dict(pos=2, type='in', action='ACCEPT', comment='SSH over nodes public addresses',
                                     source='+pve-nodes', dest='+pve-nodes', macro='SSH')
    rule3 = pvesh.firewall.rule_dict(pos=3, type='in', action='ACCEPT', comment='Ping over cluster net',
                                     source='local_network', dest='local_network', macro='Ping')
    rule4 = pvesh.firewall.rule_dict(pos=4, type='in', action='ACCEPT', comment='Ping over nodes public addresses',
                                     source='+pve-nodes', dest='+pve-nodes', macro='Ping')
    pvesh.firewall.group_set('pve-cluster', '#proxner - auto generated', [rule0, rule1, rule2, rule3, rule4])

    # Security Group (rule set) 'proxner-cluster' may allow ipsec geneve tunnel traffic between gw-nodes and pve-nodes
    rule0 = pvesh.firewall.rule_dict(pos=0, type='in', action='ACCEPT', comment='IPsec traffic',
                                     source='+gw-nodes', dest='+pve-nodes', macro='IPsec')
    rule1 = pvesh.firewall.rule_dict(pos=1, type='in', action='ACCEPT', comment='Geneve tunnel traffic',
                                     source='+gw-nodes', dest='+pve-nodes', proto='udp', dport=6081)
    rule2 = pvesh.firewall.rule_dict(pos=2, type='in', action='ACCEPT', comment='SSH over nodes public addresses',
                                     source='+gw-nodes', dest='+pve-nodes', macro='SSH')
    rule3 = pvesh.firewall.rule_dict(pos=4, type='in', action='ACCEPT', comment='Ping over nodes public addresses',
                                     source='+gw-nodes', dest='+pve-nodes', macro='Ping')
    pvesh.firewall.group_set('proxner-cluster', '#proxner - auto generated', [rule0, rule1, rule2, rule3])

    # IP Set 'proxner-vnet' may describe all configured vnet networks
    ipset_vent = list()
    for vnet_name in config.vnet_names():
        net6 = config.vnet_ip6_network(vnet_name)
        if net6:
            ipset_vent.append({'cidr': "%s" % net6.cidr, 'comment': "%s IPv6" % vnet_name, 'nomatch': False})
        net4 = config.vnet_ip4_network(vnet_name)
        if net4:
            ipset_vent.append({'cidr': "%s" % net4.cidr, 'comment': "%s IPv4" % vnet_name, 'nomatch': False})
    ipset_vent = sorted(ipset_vent, key=lambda i: i['cidr'])
    pvesh.firewall.ipset_set('proxner-vnet', '#proxner - auto generated', ipset_vent)

    # Security Group (rule set) 'proxner-vnet' may allow any traffic over the vnet bridges and tunnels
    rule0 = pvesh.firewall.rule_dict(pos=0, type='in', action='ACCEPT', comment='Proxner vnet traffic',
                                     source='+proxner-vnet', dest='+proxner-vnet')
    pvesh.firewall.group_set('proxner-vnet', '#proxner - auto generated', [rule0])

    #
    # Final cluster firewall rules of proxner:
    #
    rule_pve_cluster = pvesh.firewall.rule_dict(pos=0, type='group', action='pve-cluster',
                                                comment='#proxner - auto generated')
    rule_proxner_cluster = pvesh.firewall.rule_dict(pos=1, type='group', action='proxner-cluster',
                                                    comment='#proxner - auto generated')
    rule_vnet = pvesh.firewall.rule_dict(pos=2, type='group', action='proxner-vnet',
                                         comment='#proxner - auto generated')
    pvesh.firewall.rules_maintain([rule_pve_cluster, rule_proxner_cluster, rule_vnet])

    return 0
