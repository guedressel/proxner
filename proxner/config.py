import collections
import io
import toml
import socket
import logging
import re
from netaddr import IPNetwork, IPAddress, AddrFormatError
from typing import Optional

config_data = dict()
cache = dict()
logger = logging.getLogger('config')
host_name_regex = re.compile(r"(?!-)[a-z0-9-]{1,63}(?<!-)$", re.IGNORECASE)
vnet_name_regex = re.compile(r"^([a-z_\-]+)(\d+)$", re.IGNORECASE)


def load(config_fileobj: io.IOBase) -> None:
    """
    :param config_fileobj:
    :return:
    """
    global config_data, cache
    logger.debug("Load configuration")
    config_data = toml.load(config_fileobj, _dict=collections.OrderedDict)

    logger.debug("Clear configuration cache")
    cache = dict()

    # validate
    nodes = _nodes_config()
    for node in nodes:
        if not node.get('name', '').strip():
            raise InvalidConfig("Found node without name")

    node_names_ = node_names()
    for node_name in node_names_:
        if not is_valid_hostname(node_host(node_name)):
            raise InvalidConfig("Invalid host name for node \"%s\"" % node_name)

    names = []
    for node_name in node_names_:
        if node_name in names:
            raise InvalidConfig("Duplicate node name \"%s\"" % node_name)
        names.append(node_name)

    pve_cluster_net = pve_cluster_net_addr()
    if not pve_cluster_net:
        raise InvalidConfig("Missing pve cluster network CIDR")

    pve_cluster_addresses = []
    for node_name in pve_node_names():
        pve_cluster_addr = pve_node_address_ip4(node_name)
        if not pve_cluster_addr:
            raise InvalidConfig("Missing pve cluster net address for pve node %s" % node_name)
        if pve_cluster_addr not in pve_cluster_net:
            raise InvalidConfig("PVE cluster address of node %s not within pve cluster network %s" % (node_name, pve_cluster_net))
        if pve_cluster_addr in pve_cluster_addresses:
            raise InvalidConfig("PVE cluster address of node %s is not unique" % node_name)
        pve_cluster_addresses.append(pve_cluster_addr)

    vnets = _vnet_configs()
    if vnets:
        for vnet in vnets:
            vnet_name = vnet.get('name', '')
            if not vnet_name:
                raise InvalidConfig("Found vnet without name")

            p = vnet_name_regex.match(vnet_name)
            if not p:
                raise InvalidConfig("Bad network name: %s" % vnet_name)

        for vnet_name in vnet_names():
            ip6net = vnet_ip6_network(vnet_name)
            ip4net = vnet_ip4_network(vnet_name)
            for node_name in node_names_:
                ''' Check for valid network address indices for all nodes '''
                ip6 = vnet_node_address_ip6(vnet_name, node_name)
                if ip6 and ip6 not in ip6net:
                    raise InvalidConfig("Address %s (node %s) is not within %s network %s" %
                                        (ip6, node_name, vnet_name, ip6net))
                ip4 = vnet_node_address_ip4(vnet_name, node_name)
                if ip4 and ip4 not in ip4net:
                    raise InvalidConfig("Address %s (node %s) is not within %s network %s" %
                                        (ip4, node_name, vnet_name, ip4net))

    logger.debug("Configuration validated")


GW_NODE = 'gw'
PVE_NODE = 'pve'


def is_valid_hostname(hostname: str) -> bool:
    if len(hostname) > 253:
        return False
    labels = hostname.split(".")
    # the TLD must be not all-numeric
    if re.match(r"[0-9]+$", labels[-1]):
        return False

    return all(host_name_regex.match(label) for label in labels)


def _pve_nodes_config() -> [dict]:
    return config_data.get('pve-nodes', [])


def _pve_node_config(node_name) -> Optional[dict]:
    for node in config_data.get('pve-nodes', []):
        if node.get('name') == node_name:
            return node


def _gw_nodes_config() -> [dict]:
    return config_data.get('gw-nodes', [])


def _gw_node_config(node_name) -> Optional[dict]:
    for node in config_data.get('gw-nodes', []):
        if node.get('name') == node_name:
            return node


def _nodes_config() -> [dict]:
    return _gw_nodes_config() + _pve_nodes_config()


def _node_config(node_name: str,  node_type: str=None) -> Optional[dict]:
    if node_type == PVE_NODE:
        return _pve_node_config(node_name)
    if node_type == GW_NODE:
        return _gw_node_config(node_name)
    pve = _pve_node_config(node_name)
    if pve:
        return pve
    return _gw_node_config(node_name)


def node_name_by_host(hostname: str) -> Optional[str]:
    for node_name in node_names():
        if hostname == node_host(node_name):
            return node_name


def domain():
    global cache
    if 'domain' not in cache:
        if 'domain' in config_data:
            return config_data.get('domain')
        logger.debug("No global domain name defined. Resolving system domain name as fallback")
        fqdn = socket.getfqdn()
        cache['domain'] = ".".join(fqdn.split('.')[1:])
    return cache['domain']


def pve_domain():
    return 'pve'


def proxmox_node_names() -> [str]:
    """
    Retrieve list of proxmox (pve) node names in config.
    :return: list()
    """
    return [node.get('name') for node in _pve_nodes_config()]


def gw_node_names() -> [str]:
    """
    Retrieve list of gateway (gw) node names in config.
    :return: list()
    """
    return [node.get('name') for node in _gw_nodes_config()]


def pve_node_names() -> [str]:
    """
    Retrieve list of proxmox VE (pve) node names in config.
    :return: list()
    """
    return [node.get('name') for node in _pve_nodes_config()]


def node_names() -> [str]:
    """
    Retrieve list of all (gw + pve) node names in config.
    :return: list()
    """
    global cache
    if not 'node_names' in cache:
        cache['node_names'] = [node.get('name') for node in _nodes_config()]
    return cache['node_names']


def node_index(node_name: str) -> Optional[int]:
    global cache
    if 'node_index' not in cache:
        cache['node_index'] = {name: i for i, name in enumerate(node_names())}
    if node_name not in cache['node_index']:
        logger.info("Node not found: %s" % node_name)
        return None
    return cache['node_index'][node_name]


def node_host(node_name: str) -> Optional[str]:
    global cache
    if 'node_host' not in cache:
        cache['node_host'] = dict()
    if node_name not in cache['node_host']:
        node = _node_config(node_name)
        if not node:
            logger.info("Node not found: %s" % node_name)
            return None
        if 'host' in node:
            cache['node_host'][node_name] = node.get('host')
        else:
            logger.debug("No host name defined for node %s. Using node name as fallback" % node_name)
            cache['node_host'][node_name] = node_name
    return cache['node_host'][node_name]


def node_domain(node_name: str) -> Optional[str]:
    global cache
    if 'node_domain' not in cache:
        cache['node_domain'] = dict()
    if node_name not in cache['node_domain']:
        node = _node_config(node_name)
        if not node:
            logger.info("Node not found: %s" % node_name)
            return None
        if 'domain' in node:
            cache['node_domain'][node_name] = node.get('domain')
        else:
            logger.debug("No domain name defined for node %s. Using global domain as fallback" % node_name)
            cache['node_domain'][node_name] = domain()
    return cache['node_domain'][node_name]


def node_fqdn(node_name: str) -> Optional[str]:
    # type (str) -> str
    h = node_host(node_name)
    d = node_domain(node_name)
    if h and d:
        return "%s.%s" % (h, d)


def node_cluster_fqdn(node_name: str) -> Optional[str]:
    h = node_host(node_name)
    d = pve_domain()
    if h and d:
        return "%s.%s" % (h, d)


def node_ip6(node_name: str) -> Optional[str]:
    """
    Retrieve IPv6 address of an node config.
    TODO: refactor to return IPAddress object.

    :param node_name: Name of node
    :return: IPv6 address
    """
    global cache
    if 'node_ip6' not in cache:
        cache['node_ip6'] = dict()
    if node_name not in cache['node_ip6']:
        node = _node_config(node_name)
        if not node:
            logger.info("Node not found: %s" % node_name)
            return None
        if 'pubIP6' in node:
            cache['node_ip6'][node_name] = node.get('pubIP6', None)
        else:
            logger.debug("No pubIP6 defined for node %s. Resolving address dynamically" % node_name)
            try:
                result = socket.getaddrinfo(node_fqdn(node_name), 0, socket.AF_INET6)
                cache['node_ip6'][node_name] = result[0][4][0]
            except socket.gaierror as e:
                logger.error("Failed to resolve address of node %s: %s" % (node_name, e))
                return None
    return cache['node_ip6'][node_name]


def node_ip4(node_name: str) -> Optional[str]:
    """
    Retrieve IPv4 address of an node config.
    TODO: refactor to return IPAddress object.

    :param node_name: Name of node
    :return: IPv4 address
    """
    global cache
    if 'node_ip4' not in cache:
        cache['node_ip4'] = dict()
    if node_name not in cache['node_ip4']:
        node = _node_config(node_name)
        if not node:
            logger.info("Node not found: %s" % node_name)
            return None
        if 'pubIP4' in node:
            cache['node_ip4'][node_name] = node.get('pubIP4', None)
        else:
            logger.debug("No pubIP4 defined for node %s. Resolving address dynamically" % node_name)
            try:
                result = socket.getaddrinfo(node_fqdn(node_name), 0, socket.AF_INET)
                cache['node_ip4'][node_name] = result[0][4][0]
            except socket.gaierror as e:
                logger.error("Failed to resolve address of node %s: %s" % (node_name, e))
                return None
    return cache['node_ip4'][node_name]


def node_ipsec_half_secret(node_name: str) -> Optional[str]:
    node = _node_config(node_name)
    if not node:
        logger.info("Node not found: %s" % node_name)
        return None
    if 'ipsecHalfSecret' in node:
        return node.get('ipsecHalfSecret')


ipsec_conn_defaults = {
    'ikelifetime': '60m',
    'keylife': '20m',
    'rekeymargin': '3m',
    'keyingtries': 1,
    'authby': 'secret',
    'mobike': 'no',
    'keyexchange': 'ikev2',
    'ike': 'aes256-sha512-modp4096!',
    'esp': 'aes256-sha512-modp4096!',
    'type': 'transport',
    'auto': 'start'
}


def ipsec_conn_directives() -> dict:
    conf_directives = config_data.get('ipsec-connection-directives', {})
    return {**ipsec_conn_defaults, **conf_directives}
    # see https://treyhunner.com/2016/02/how-to-merge-dictionaries-in-python/


def _vnet_configs() -> [dict]:
    return config_data.get('vnet', [])


def _vnet_config(vnet_name: str) -> dict:
    for vnet in _vnet_configs():
        n = vnet.get('name', '').strip()
        if n and n == vnet_name:
            return vnet


def vnet_names() -> [str]:
    global cache
    if 'vnet_names' not in cache:
        cache['vnet_names'] = [vnet.get('name') for vnet in _vnet_configs()]
    return cache['vnet_names']


def vnet_defined(vnet_name: str) -> bool:
    return vnet_name in vnet_names()


def vnet_ip6_network(vnet_name: str) -> Optional[IPNetwork]:
    global cache
    if 'vnet_ip6_network' not in cache:
        cache['vnet_ip6_network'] = dict()
    if vnet_name not in cache['vnet_ip6_network']:
        c = _vnet_config(vnet_name)
        if 'netIP6' not in c:
            logger.info("No IPv6 address defined for vnet %s" % vnet_name)
            return None
        cache['vnet_ip6_network'][vnet_name] = IPNetwork(c.get('netIP6'), version=6)
    return cache['vnet_ip6_network'][vnet_name]


def vnet_ip4_network(vnet_name: str) -> Optional[IPNetwork]:
    global cache
    if 'vnet_ip4_network' not in cache:
        cache['vnet_ip4_network'] = dict()
    if vnet_name not in cache['vnet_ip4_network']:
        c = _vnet_config(vnet_name)
        if 'netIP4' not in c:
            logger.info("No IPv4 address defined for vnet %s" % vnet_name)
            return None
        cache['vnet_ip4_network'][vnet_name] = IPNetwork(c.get('netIP4'), version=4)
    return cache['vnet_ip4_network'][vnet_name]


def vnet_excluded_nodes(vnet_name: str) -> [str]:
    # type (str) -> [str]
    global cache
    if 'vnet_excluded_nodes' not in cache:
        cache['vnet_excluded_nodes'] = dict()
    if vnet_name not in cache['vnet_excluded_nodes']:
        c = _vnet_config(vnet_name)
        if not c:
            return []
        node_names_ = node_names()
        cache['vnet_excluded_nodes'][vnet_name] = [n for n in c.get('exclude', []) if n in node_names_]
    return cache['vnet_excluded_nodes'][vnet_name]


# def vnet_address_index(vnet_name, node_name):
#     global cache
#     if 'vnet_address_index' not in cache:
#         cache['vnet_address_index'] = dict()
#     if vnet_name not in cache['vnet_address_index']:
#         cache['vnet_address_index'][vnet_name] = dict()
#     if node_name not in cache['vnet_address_index'][vnet_name]:
#         index = _node_config(node_name).get('vnetAddressIndex')
#         if isinstance(index, dict):
#             if vnet_name in index:
#                 cache['vnet_address_index'][vnet_name][node_name] = int(index['vnet_name'])
#             else:
#                 cache['vnet_address_index'][vnet_name][node_name] = int(index['default'])
#         else:
#             cache['vnet_address_index'][vnet_name][node_name] = int(index)
#     return cache['vnet_address_index'][vnet_name][node_name]
#

def vnet_node_address_ip6(vnet_name: str, node_name: str) -> Optional[IPAddress]:
    global cache
    if 'vent_node_address_ip6' not in cache:
        cache['vent_node_address_ip6'] = dict()
    if vnet_name not in cache['vent_node_address_ip6']:
        cache['vent_node_address_ip6'][vnet_name] = dict()
    if node_name not in cache['vent_node_address_ip6'][vnet_name]:
        vnet_addresses = _vnet_config(vnet_name).get('IP6', {})
        node_address = vnet_addresses.get(node_name, None)
        if node_address:
            cache['vent_node_address_ip6'][vnet_name][node_name] = IPAddress(node_address, version=6)
        else:
            cache['vent_node_address_ip6'][vnet_name][node_name] = None
    return cache['vent_node_address_ip6'][vnet_name][node_name]


def vnet_node_address_ip4(vnet_name: str, node_name: str) -> Optional[IPAddress]:
    global cache
    if 'vent_node_address_ip4' not in cache:
        cache['vent_node_address_ip4'] = dict()
    if vnet_name not in cache['vent_node_address_ip4']:
        cache['vent_node_address_ip4'][vnet_name] = dict()
    if node_name not in cache['vent_node_address_ip4'][vnet_name]:
        vnet_addresses = _vnet_config(vnet_name).get('IP4', {})
        node_address = vnet_addresses.get(node_name, None)
        if node_address:
            cache['vent_node_address_ip4'][vnet_name][node_name] = IPAddress(node_address, version=4)
        else:
            cache['vent_node_address_ip4'][vnet_name][node_name] = None
    return cache['vent_node_address_ip4'][vnet_name][node_name]


def pve_cluster_net_name() -> str:
    return 'pve'


def pve_cluster_net_addr() -> Optional[IPNetwork]:
    try:
        net = config_data.get('clusterNet4', '')
        return IPNetwork(net, version=4)
    except AddrFormatError:
        return None


def pve_node_address_ip4(node_name: str) -> Optional[IPAddress]:
    global cache
    if 'pve_cluster_addresses_ip4' not in cache:
        cache['pve_cluster_addresses_ip4'] = dict()
    if node_name not in cache['pve_cluster_addresses_ip4']:
        cluster_addr = _pve_node_config(node_name).get('clusterIP4', "")
        if cluster_addr:
            cache['pve_cluster_addresses_ip4'][node_name] = IPAddress(cluster_addr, version=4)
        else:
            cache['pve_cluster_addresses_ip4'][node_name] = None
    return cache['pve_cluster_addresses_ip4'][node_name]


def pve_management_net() -> [(str, IPNetwork)]:
    global cache
    if 'pve_management_net' not in cache:
        networks = config_data.get('pve-management-networks', {})
        cache['pve_management_net'] = list()
        for comment, cidr in networks.items():
            cache['pve_management_net'].append((comment, IPNetwork(cidr)))
    return cache['pve_management_net']



class InvalidConfig(Exception):
    pass
