# -*- coding: utf-8 -*-

import jinja2


env = jinja2.Environment(loader=jinja2.PackageLoader('proxner'))


def render(template_name, data):
    template = env.get_template(template_name)
    return template.render(data)
