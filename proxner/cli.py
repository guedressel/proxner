
import sys
import os
import argparse
import logging
import socket
from proxner.commands import ipsec, vnet, ifupdown, conf, rollout, hosts, pve
from proxner import config

logger = logging.getLogger('proxner')


def main():
    parser = argparse.ArgumentParser(description='Proxner - Proxmox fits Hetzner.')
    parser.add_argument('--verbose', '-v', action='count', default=0)
    parser.add_argument('--quiet', '-q', action='store_true', default=False)
    parser.add_argument('--config', '-c', dest='config_file',
                        help='Proxner configuration file')
    parser.add_argument('--node', '-n',
                        help='Node to execute command for (default: auto detect by host name)')

    command_parsers = parser.add_subparsers(dest='command', help='Command')
    command_parsers.required = True

    conf_parser = command_parsers.add_parser('conf')
    conf.init_argument_parser(conf_parser)

    ipsec_parser = command_parsers.add_parser('ipsec')
    ipsec.init_argument_parser(ipsec_parser)

    hosts_parser = command_parsers.add_parser('hosts')
    hosts.init_argument_parser(hosts_parser)

    vnet_parser = command_parsers.add_parser('vnet')
    vnet.init_argument_parser(vnet_parser)

    ifupdown_parser = command_parsers.add_parser('ifupdown')
    ifupdown.init_argument_parser(ifupdown_parser)

    rollout_parser = command_parsers.add_parser('rollout')
    rollout.init_argument_parser(rollout_parser)

    pve_parser = command_parsers.add_parser('pve')
    pve.init_argument_parser(pve_parser)

    cli_args = sys.argv[1:]
    parsed_args = parser.parse_args(cli_args)

    if parsed_args.verbose >= 2:
        logging.basicConfig(level=logging.DEBUG)
    elif parsed_args.verbose == 1:
        logging.basicConfig(level=logging.INFO)
    else:
        logging.basicConfig(level=logging.WARN)
    if parsed_args.quiet:
        logging.disable(logging.CRITICAL)

    default_config_file_locations = [
        '/etc/pve/proxner/proxner.conf',
        '/etc/proxner/proxner.conf',
        '/etc/proxner/config.toml',
    ]
    config_file = None
    if parsed_args.config_file:
        if not os.path.exists(parsed_args.config_file):
            logger.critical("Configuration file not found: %s" % parsed_args.config_file)
            return 1
        config_file = parsed_args.config_file
    else:
        for path in default_config_file_locations:
            if os.path.exists(path):
                config_file = path
                break

    if config_file:
        try:
            logger.info("Loading configuration: %s" % config_file)
            with open(config_file) as cf:
                config.load(cf)
        except config.InvalidConfig as e:
            logger.fatal(e)
            return 1
        except Exception as e:
            logger.fatal("Failed to load configuration", e)
            return 1
    else:
        logger.warning("No configuration loaded")

    if not parsed_args.node:
        hostname = socket.gethostname()
        local_node = config.node_name_by_host(hostname)
        if local_node:
            parsed_args.node = local_node
        else:
            logger.fatal('No node configuration found for host name %s' % hostname)
            return 1
    else:
        if parsed_args.node not in config.node_names():
            logger.fatal('Node configuration not found for %s' % parsed_args.node)
            return 1

    if parsed_args.command == 'conf':
        return conf.run(parsed_args)
    elif parsed_args.command == 'ipsec':
        return ipsec.run(parsed_args)
    elif parsed_args.command == 'hosts':
        return hosts.run(parsed_args)
    elif parsed_args.command == 'vnet':
        return vnet.run(parsed_args)
    elif parsed_args.command == 'ifupdown':
        return ifupdown.run(parsed_args)
    elif parsed_args.command == 'rollout':
        return rollout.run(parsed_args)
    elif parsed_args.command == 'pve':
        return pve.run(parsed_args)
    else:
        logger.fatal("Invalid command %s" % parsed_args.command)


if __name__ == '__main__':

    sys.exit(main())
