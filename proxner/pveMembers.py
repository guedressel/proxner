
import json



pve_members_file = "/etc/pve/.members"


node_names = []
with open(pve_members_file) as f:
    pve_members = json.load(f)
    for node in pve_members['nodelist'].keys():
        node_names.append(node)

if not node_names:
    eprint("PVE node list is empty")
    sys.exit(1)
