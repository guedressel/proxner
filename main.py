#! /usr/bin/env python3

if __name__ == '__main__':
    import sys
    import proxner.cli as cli
    sys.exit(cli.main())
