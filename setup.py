from setuptools import setup, find_packages

setup(
    name='Proxner',
    version='0.1-dev',
    license='GNU General Public License v3 or later (GPLv3+)',
    description="Utilities to manage Proxmox VE on Hetzner hosts.",
    long_description=open('README.txt').read(),
    author='guedressel',
    author_email='guedressel@blackened.link',
    url='example.com',
    project_urls={
        "Bug Tracker": "https://bugs.example.com/Proxner/",
        "Documentation": "https://docs.example.com/Proxner/",
        "Source Code": "https://code.example.com/Proxner/",
    },
    classifiers=[
        'Development Status :: 3 - Alpha'
        'Environment :: Console',
        'Intended Audience :: System Administrators',
        'Operating System :: POSIX :: Linux',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Topic :: System :: Clustering',
        'Topic :: System :: Networking',
        'Topic :: System :: Systems Administration'
    ],
    install_requires=[
        "toml >= 0.9.1",
        "Jinja2 >= 2.8",
        "netaddr >= 0.7.18",
        "debinterface >= 3.4.0"
    ],
    packages=find_packages(),
    package_data={
        'proxner': ['templates/*.jinja']
    },
    entry_points={
        'console_scripts': [
            'proxner = proxner.cli:main'
        ]
    },
)
